package noah.ailab.mini1;



public class Date {
    private String DOW,DOM;
    private int state;

    public Date(String DOW, String DOM, int state) {
        this.DOW = DOW;
        this.DOM = DOM;
        this.state = state;
    }

    public Date() {
    }

    public Date(String DOW, String DOM) {
        this.DOW = DOW;
        this.DOM = DOM;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getDOW() {
        return DOW;
    }

    public void setDOW(String DOW) {
        this.DOW = DOW;
    }

    public String getDOM() {
        return DOM;
    }

    public void setDOM(String DOM) {
        this.DOM = DOM;
    }
}
