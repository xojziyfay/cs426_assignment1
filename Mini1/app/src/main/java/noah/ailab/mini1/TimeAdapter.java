package noah.ailab.mini1;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.MyViewHolder> {

    private List<Time> timeList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TIME;

        public MyViewHolder(View view) {
            super(view);
            TIME = (TextView) view.findViewById(R.id.time);

        }
    }


    public TimeAdapter(List<Time> timeList) {
        this.timeList = timeList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Time time = timeList.get(position);
        holder.TIME.setText(time.getTime());
        if(time.getState()==1){
            /* holder.DOM.setBackgroundColor(Color.RED);*/
            holder.TIME.setBackgroundResource(R.drawable.date_chosen);
        }
    }

    @Override
    public int getItemCount() {
        return timeList.size();
    }
}
