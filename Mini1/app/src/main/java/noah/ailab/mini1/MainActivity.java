package noah.ailab.mini1;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends AppCompatActivity {

    ImageView next;
    private ListView CineList;

    private List<Date> dateList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DateAdapter mAdapter;

    private List<Time> timeList = new ArrayList<>();
    private RecyclerView Timetest;
    private TimeAdapter timeAdapter;

    int curDatePos=-1;
    String curDate="29";


    private RecyclerView recyclerViewParent;
    private ArrayList<Cinemas> parentChildObj=new ArrayList<>();
    private CinemasAdapter parentAdapter;

    @Override

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main_hor);
            recyclerViewParent = (RecyclerView) findViewById(R.id.Cine);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewParent.setLayoutManager(manager);
            recyclerViewParent.setHasFixedSize(true);
            createData();
            final CinemasAdapter parentAdapter = new CinemasAdapter(this,parentChildObj);

            recyclerViewParent.setAdapter(parentAdapter);
            parentAdapter.notifyDataSetChanged();


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            next= (ImageView) findViewById(R.id.nextBtn);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    int r=parentAdapter.getCrow();
                    int c=parentAdapter.getCcolum();


                    if(r!=-1 && c!=-1) {
                        Intent intent=new Intent(MainActivity.this, SeatActivity.class);
                        String timeSend=parentChildObj.get(r).getTime(c).getTime();
                        String dateSend=curDate;
                        String cinemasSend=parentChildObj.get(r).getName();

                        Bundle bundle =new Bundle();
                        bundle.putString("timeSent",timeSend);
                        bundle.putString("dateSent",dateSend);
                        bundle.putString("cineSent",cinemasSend);

                        intent.putExtra("FilmData",bundle);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Choose your ticket info", Toast.LENGTH_SHORT).show();
                    }


                }
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

            mAdapter = new DateAdapter(dateList);
            /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());*/
            RecyclerView.LayoutManager mLayoutManager =
                    new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(),DividerItemDecoration.HORIZONTAL));
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Date dateC = dateList.get(position);
                    curDate=dateC.getDOM();
                    dateC.setState(1);

                    dateList.set(position,dateC);

                    if (curDatePos>=0) {
                        Date dateCur = dateList.get(curDatePos);
                        dateCur.setState(0);
                        dateList.set(curDatePos,dateCur);
                    }
                    curDatePos=position;
                    Toast.makeText(getApplicationContext(), dateC.getDOM() + " is selected!", Toast.LENGTH_SHORT).show();
                    mAdapter.notifyDataSetChanged();
                    recyclerView.invalidateItemDecorations();
                    recyclerView.setAdapter(mAdapter);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
            recyclerView.setAdapter(mAdapter);
            prepareMovieData();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main);
            recyclerViewParent = (RecyclerView) findViewById(R.id.Cine);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewParent.setLayoutManager(manager);
            recyclerViewParent.setHasFixedSize(true);
            createData();
            final CinemasAdapter parentAdapter = new CinemasAdapter(this,parentChildObj);

            recyclerViewParent.setAdapter(parentAdapter);
            parentAdapter.notifyDataSetChanged();


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            next= (ImageView) findViewById(R.id.nextBtn);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    int r=parentAdapter.getCrow();
                    int c=parentAdapter.getCcolum();


                    if(r!=-1 && c!=-1) {
                        Intent intent=new Intent(MainActivity.this, SeatActivity.class);
                        String timeSend=parentChildObj.get(r).getTime(c).getTime();
                        String dateSend=curDate;
                        String cinemasSend=parentChildObj.get(r).getName();

                        Bundle bundle =new Bundle();
                        bundle.putString("timeSent",timeSend);
                        bundle.putString("dateSent",dateSend);
                        bundle.putString("cineSent",cinemasSend);

                        intent.putExtra("FilmData",bundle);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Choose your ticket info", Toast.LENGTH_SHORT).show();
                    }


                }
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

            mAdapter = new DateAdapter(dateList);
            /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());*/
            RecyclerView.LayoutManager mLayoutManager =
                    new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(),DividerItemDecoration.HORIZONTAL));
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Date dateC = dateList.get(position);
                    curDate=dateC.getDOM();
                    dateC.setState(1);

                    dateList.set(position,dateC);

                    if (curDatePos>=0) {
                        Date dateCur = dateList.get(curDatePos);
                        dateCur.setState(0);
                        dateList.set(curDatePos,dateCur);
                    }
                    curDatePos=position;
                    Toast.makeText(getApplicationContext(), dateC.getDOM() + " is selected!", Toast.LENGTH_SHORT).show();
                    mAdapter.notifyDataSetChanged();
                    recyclerView.invalidateItemDecorations();
                    recyclerView.setAdapter(mAdapter);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
            recyclerView.setAdapter(mAdapter);
            prepareMovieData();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

/*
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_hor);
        }*/



        recyclerViewParent = (RecyclerView) findViewById(R.id.Cine);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewParent.setLayoutManager(manager);
        recyclerViewParent.setHasFixedSize(true);
        createData();
        final CinemasAdapter parentAdapter = new CinemasAdapter(this,parentChildObj);

        recyclerViewParent.setAdapter(parentAdapter);
        parentAdapter.notifyDataSetChanged();


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        next= (ImageView) findViewById(R.id.nextBtn);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                int r=parentAdapter.getCrow();
                int c=parentAdapter.getCcolum();


                if(r!=-1 && c!=-1) {
                    Intent intent=new Intent(MainActivity.this, SeatActivity.class);
                String timeSend=parentChildObj.get(r).getTime(c).getTime();
                String dateSend=curDate;
                String cinemasSend=parentChildObj.get(r).getName();

                Bundle bundle =new Bundle();
                bundle.putString("timeSent",timeSend);
                bundle.putString("dateSent",dateSend);
                bundle.putString("cineSent",cinemasSend);

                intent.putExtra("FilmData",bundle);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Choose your ticket info", Toast.LENGTH_SHORT).show();
                }


            }
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new DateAdapter(dateList);
        /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());*/
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(),DividerItemDecoration.HORIZONTAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               Date dateC = dateList.get(position);
               curDate=dateC.getDOM();
                dateC.setState(1);

                dateList.set(position,dateC);

                if (curDatePos>=0) {
                    Date dateCur = dateList.get(curDatePos);
                    dateCur.setState(0);
                    dateList.set(curDatePos,dateCur);
                }
                curDatePos=position;
                Toast.makeText(getApplicationContext(), dateC.getDOM() + " is selected!", Toast.LENGTH_SHORT).show();
                mAdapter.notifyDataSetChanged();
                recyclerView.invalidateItemDecorations();
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerView.setAdapter(mAdapter);
        prepareMovieData();




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*        Timetest = (RecyclerView) findViewById(R.id.timetest);

        timeAdapter = new TimeAdapter(timeList);
        *//*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());*//*
        RecyclerView.LayoutManager mLayoutManager2 =
                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        Timetest.setLayoutManager(mLayoutManager2);
        Timetest.setItemAnimator(new DefaultItemAnimator());
        Timetest.addItemDecoration(new DividerItemDecoration(getBaseContext(),DividerItemDecoration.HORIZONTAL));
        Timetest.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), Timetest, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Time dateC = timeList.get(position);

                Toast.makeText(getApplicationContext(), dateC.getTime() + " is selected!", Toast.LENGTH_SHORT).show();
                mAdapter.notifyDataSetChanged();
                recyclerView.invalidateItemDecorations();
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        Timetest.setAdapter(timeAdapter);
        prepareTime();*/
////////////////////////////////////////////////////////////////////////////////////////////////
    }
    private void prepareMovieData() {
        Date date = new Date("Fir","12",0);
        dateList.add(date);

        date = new Date("Sat","13",0);
        dateList.add(date);

        date = new Date("Sat","14",0);
        dateList.add(date);
        date = new Date("Sat","15",0);
        dateList.add(date);
        date = new Date("Sat","16",0);
        dateList.add(date);
        date = new Date("Sat","17",0);
        dateList.add(date);
        date = new Date("Sat","18",0);
        dateList.add(date);
        date = new Date("Sat","19",0);
        dateList.add(date);
        date = new Date("Sat","20",0);
        dateList.add(date);
        date = new Date("Sat","21",0);
        dateList.add(date);
        date = new Date("Sat","22",0);
        dateList.add(date);

        mAdapter.notifyDataSetChanged();
    }
    private void prepareTime() {
        Time time = new Time("12:00AM",1);
        timeList.add(time);

        time = new Time("13:00PM",0);
        timeList.add(time);

        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);
        time = new Time("13:00PM",0);
        timeList.add(time);

        timeAdapter.notifyDataSetChanged();
    }
    private void createData() {
        /*parentChildObj = new ArrayList<>();*/
        ArrayList<Time> list1 = new ArrayList<Time>();
        ArrayList<Time> list2 = new ArrayList<Time>();
        ArrayList<Time> list3 = new ArrayList<Time>();
        ArrayList<Time> list4 = new ArrayList<Time>();
        ArrayList<Time> list5 = new ArrayList<Time>();
        for (int i = 0; i < 20; i++) {
            Time c1 = new Time(String.valueOf(i)+":00",0);
            list1.add(c1);
        }

        for (int i = 0; i < 24; i++) {
            Time c2 = new Time(String.valueOf(i)+":00",0);
            list2.add(c2);
        }
        for (int i = 0; i < 24; i++) {
            Time c3 = new Time(String.valueOf(i)+":00",0);
            list3.add(c3);
        }
        for (int i = 0; i < 24; i++) {
            Time c4 = new Time(String.valueOf(i)+":00",0);
            list4.add(c4);
        }
        for (int i = 0; i < 24; i++) {
            Time c5 = new Time(String.valueOf(i)+":00",0);
            list5.add(c5);
        }

        Cinemas pc1 = new Cinemas("Lotte",list1);
        pc1.setChildren(list1);
        parentChildObj.add(pc1);

        Cinemas pc2 = new Cinemas("Hung Vuong",list1);
        pc2.setChildren(list2);
        parentChildObj.add(pc2);
        Cinemas pc3 = new Cinemas("Camellia",list1);
        pc3.setChildren(list3);
        parentChildObj.add(pc3);
        Cinemas pc4 = new Cinemas("Carina",list1);
        pc4.setChildren(list4);
        parentChildObj.add(pc4);
        Cinemas pc5 = new Cinemas("Galaxy",list1);
        pc5.setChildren(list5);
        parentChildObj.add(pc5);
    }


}
