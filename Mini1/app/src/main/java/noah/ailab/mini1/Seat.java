package noah.ailab.mini1;
public class Seat {
    private int states;

    public Seat(int sta) {
        this.states=sta;
    }

    public int getStates() {
        return states;
    }

    public void setStates(int States) {
        this.states = States;
    }
}
