package noah.ailab.mini1;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/*import com.mkyong.android.R;*/

public class SeatAdapter extends BaseAdapter {
    private Context context;
    private final Seat[] seat;

    public SeatAdapter(Context context, Seat[] SEAT) {
        this.context = context;
        this.seat = SEAT;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.seat, null);
            TextView btn = (TextView) gridView
                    .findViewById(R.id.seat_btn);
            if(seat[position].getStates()==2){
                btn.setBackgroundColor(Color.CYAN);

            }
            else if (seat[position].getStates()==1){
                btn.setBackgroundColor(Color.RED);

            }




        } else {
            gridView = inflater.inflate(R.layout.seat, null);
            TextView btn = (TextView) gridView
                    .findViewById(R.id.seat_btn);
            if(seat[position].getStates()==2){
                btn.setBackgroundColor(Color.CYAN);

            }
            else if (seat[position].getStates()==1){
                btn.setBackgroundColor(Color.RED);
            }
            gridView = (View) convertView;
        }

        return gridView;
    }


    @Override
    public int getCount() {
        return seat.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}