package noah.ailab.mini1;

import java.util.ArrayList;

public class Cinemas {
    private String name;
    private ArrayList<Time> children;

    public Cinemas(String name, ArrayList<Time> children) {
        this.name = name;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Time> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Time> children) {
        this.children = children;
    }
    public Time getTime(int position){
        return children.get(position);
    }
}