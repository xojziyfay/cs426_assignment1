package noah.ailab.mini1;

public class Time {
    private String time;
    private int state;

    public Time(String time, int state) {
        this.time = time;
        this.state = state;
    }

    public Time(String time) {
        this.time = time;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
