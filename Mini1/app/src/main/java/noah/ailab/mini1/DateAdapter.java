package noah.ailab.mini1;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.MyViewHolder> {

    private List<Date> dateList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView DOW,DOM;

        public MyViewHolder(View view) {
            super(view);
            DOW = (TextView) view.findViewById(R.id.dofw);
            DOM = (TextView) view.findViewById(R.id.dofm);

        }
    }


    public DateAdapter(List<Date> dateList) {
        this.dateList = dateList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        /*TextView btn = (TextView) itemView
                .findViewById(R.id.dofm);
        btn.setBackgroundColor(Color.RED);*/


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Date date = dateList.get(position);
        holder.DOW.setText(date.getDOW());
        holder.DOM.setText(date.getDOM());
        if(date.getState()==1){
           /* holder.DOM.setBackgroundColor(Color.RED);*/
            holder.DOM.setBackgroundResource(R.drawable.date_chosen);
        }
    }

    @Override
    public int getItemCount() {
        return dateList.size();
    }
}
