package noah.ailab.mini1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class SeatActivity extends AppCompatActivity {

    GridView gridView;
    int curSeatChosen=0;
    Seat[] seat=new Seat[100];
    SeatAdapter adapter=new SeatAdapter(this, seat);
    TextView order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*int orientation=this.getResources().getConfiguration().orientation;
        if(orientation== Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_seat);
        }*/
        setContentView(R.layout.activity_seat);
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("FilmData");
        String date=bundle.getString("dateSent");
        String time=bundle.getString("timeSent");
        String cine=bundle.getString("cineSent");

        TextView order=(TextView) findViewById(R.id.order);
        TextView timedis=(TextView) findViewById(R.id.timeShow);
        TextView datedis=(TextView) findViewById(R.id.date);
        TextView cinedis=(TextView) findViewById(R.id.cinemas);


        timedis.setText(time);
        datedis.setText(date);
        cinedis.setText(cine);
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"You ordered "+curSeatChosen, Toast.LENGTH_SHORT).show();
            }
        });


        for(int i=0;i<100;i++){
            Random r = new Random();
            int s= r.nextInt(2 - 0) + 0;
            seat[i]=new Seat(s);
        }


        gridView = (GridView) findViewById(R.id.gridView1);
        /*gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Chosen", Toast.LENGTH_SHORT).show();
            }
        });*/
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                if(seat[position].getStates()!=1) {
                    Toast.makeText(getApplicationContext(),"Chosen"+position, Toast.LENGTH_SHORT).show();
                    if(seat[position].getStates()==0)
                    {
                        seat[position].setStates(2);
                        curSeatChosen=curSeatChosen+1;
                    }
                    else{
                        seat[position].setStates(0);
                        curSeatChosen=curSeatChosen-1;
                    }

                    adapter.notifyDataSetChanged();
                    /*gridView.invalidateViews();*/
                    gridView.setAdapter(adapter);

                    /*gridView.invalidateViews();*/
                }
                else if(seat[position].getStates()==1){
                    Toast.makeText(getApplicationContext(),"Seat not avaiable",Toast.LENGTH_SHORT).show();
                }

                /*gridView.setAdapter(new SeatAdapter(this,seat));*/


            }
        });

        gridView.setAdapter(adapter);






    }
}
