package noah.ailab.mini1;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Welcome on 8/27/2016.
 */
public class CinemasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<Cinemas> listCinemas;
    Context ctx;
    int Crow=-1;
    int Ccolum=-1;
    CinemasAdapter tm=this;
    public int getCrow(){
        return Crow;
    }
    public int getCcolum(){
        return Ccolum;
    }

    public CinemasAdapter(Context ctx, ArrayList<Cinemas> listCinemas) {
        this.ctx = ctx;
        this.listCinemas = listCinemas;


    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        public RecyclerView rv_child= itemView.findViewById(R.id.list_child);;
        public TextView CineName=itemView.findViewById(R.id.cinemasName);
        public ViewHolder(View itemView) {
            super(itemView);
            TextView CineName = (TextView) itemView.findViewById(R.id.cinemasName);
            RecyclerView rv_child= itemView.findViewById(R.id.list_child);

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cinemas, parent, false);
        /*CinemasAdapter.ViewHolder pavh = new CinemasAdapter(itemLayoutView);
        return pavh;*/
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        Cinemas p = listCinemas.get(position);
        vh.CineName.setText(p.getName());
        initChildLayoutManager(vh.rv_child, p.getChildren(), position);
    }

    private void initChildLayoutManager(final RecyclerView rv_child, ArrayList<Time> childData, final int row_pos) {
        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL,false));
        final TimeAdapter childAdapter = new TimeAdapter(childData);
        rv_child.addOnItemTouchListener(new RecyclerTouchListener(ctx, rv_child, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                int state=listCinemas.get(row_pos).getTime(position).getState();
                Toast.makeText(ctx, " is selected! row "+row_pos+" colum "+position+" state "+state, Toast.LENGTH_SHORT).show();
                if(state==1){
                    listCinemas.get(row_pos).getTime(position).setState(0);
                    Crow=-1;
                    Ccolum=-1;
                }
                else{
                    listCinemas.get(row_pos).getTime(position).setState(1);
                    if(Crow!=-1 && Ccolum!=-1 && Crow!=row_pos && Ccolum!=position){
                        listCinemas.get(Crow).getTime(Ccolum).setState(0);
                    }
                    Crow=row_pos;
                    Ccolum=position;
                }
                childAdapter.notifyDataSetChanged();


                rv_child.invalidateItemDecorations();
                rv_child.setAdapter(childAdapter);
                tm.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return listCinemas.size();
    }


}